<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package macleods-furniture
 */

/**
 * Register a custom post type called "search grant".
 *
 * @see get_post_type_labels() for label keys.
 */
function macleods_furniture_register_post_type_init() {
    $post_types['testimonial'] = [
        'labels'             => [
            'name'                  => _x( 'Testimonials', 'Post type general name', 'macleods-furniture' ),
            'singular_name'         => _x( 'Testimonial', 'Post type singular name', 'macleods-furniture' ),
            'menu_name'             => _x( 'Testimonial', 'Admin Menu text', 'macleods-furniture' ),
            'name_admin_bar'        => _x( 'Testimonial', 'Add New on Toolbar', 'macleods-furniture' ),
            'add_new'               => __( 'Add New', 'macleods-furniture' ),
            'add_new_item'          => __( 'Add New Testimonial', 'macleods-furniture' ),
            'new_item'              => __( 'New Testimonial', 'macleods-furniture' ),
            'edit_item'             => __( 'Edit Testimonial', 'macleods-furniture' ),
            'view_item'             => __( 'View Testimonial', 'macleods-furniture' ),
            'all_items'             => __( 'All Testimonial', 'macleods-furniture' ),
            'search_items'          => __( 'Search Testimonial', 'macleods-furniture' ),
            'parent_item_colon'     => __( 'Parent Testimonial:', 'macleods-furniture' ),
            'not_found'             => __( 'No Testimonial found.', 'macleods-furniture' ),
            'not_found_in_trash'    => __( 'No Testimonial found in Trash.', 'macleods-furniture' ),
            'featured_image'        => _x( 'Testimonial Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'macleods-furniture' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'macleods-furniture' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'macleods-furniture' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'macleods-furniture' ),
            'archives'              => _x( 'Testimonial archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'macleods-furniture' ),
            'insert_into_item'      => _x( 'Insert into Testimonial', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'macleods-furniture' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this Testimonial', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'macleods-furniture' ),
            'filter_items_list'     => _x( 'Filter Testimonial list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'macleods-furniture' ),
            'items_list_navigation' => _x( 'Testimonial list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'macleods-furniture' ),
            'items_list'            => _x( 'Testimonial list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'macleods-furniture' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => [ 'slug' => 'testimonial', 'with_front' => false ],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => [ 'title', 'editor', 'page-attributes' ],
        'menu_icon'          => 'dashicons-format-chat'
    ];
    
    if( $post_types ) {
        // loop through all post-types
        foreach( $post_types as $post_type => $args ) {
            register_post_type( $post_type, $args );
        }
    }
}

add_action( 'init', 'macleods_furniture_register_post_type_init' );