<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package macleods-furniture
 */

/*
 * Remove default WP builtin actions
 */
remove_action( 'wp_head', 'wp_generator' );

function macleods_furniture_head_func() {
	?>
	<meta name="msapplication-tap-highlight" content="no">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	
	<?php
	// add prerender for speed
	if( 'post' == get_post_type() ) {
		$next_article_id = macleods_furniture_get_next_article_id();
		
		echo '<link rel="prerender" href="'. get_permalink( $next_article_id ) .'">';
	}
}
add_action( 'wp_head', 'macleods_furniture_head_func' );

/*
 * @desc Localize wp-ajax
 */
function macleods_furniture_init() {
	wp_enqueue_script( 'macleods-furniture-request-script', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
	wp_localize_script( 'macleods-furniture-request-script', 'theme_ajax', array(
		'url'		=> admin_url( 'admin-ajax.php' ),
		'site_url' 	=> get_bloginfo('url'),
		'theme_url' => get_bloginfo('template_directory')
	) );
}
add_action( 'init', 'macleods_furniture_init' );


/**********************************
 * Help site speed
 **********************************/
if ( ! is_admin() ) {
  add_action( "wp_enqueue_scripts", "macleods_furniture_jquery_enqueue", 10 );
}

function macleods_furniture_jquery_enqueue() {
	wp_enqueue_style( 'macleods-furniture-style', get_stylesheet_uri() );
	// wp_enqueue_style( 'macleods-furniture-font-style', '//fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap' );

	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true );

	if( is_front_page() ) {
		// slick js
		wp_enqueue_script( 'slick-scripst-js', get_template_directory_uri() . '/js/slick.min.js', false, '1.8.1', true );
	}
	
	wp_enqueue_script( 'macleods-furniture-scripts', get_template_directory_uri() . '/js/scripts.js', ['jquery'], '1.0', true );
	wp_enqueue_script( 'macleods-furniture-sticky-scripts', get_template_directory_uri() . '/js/sticky.min.js', ['jquery'], '1.1.3', true );
	wp_enqueue_script( 'macleods-furniture-lazysizes-scripts', get_template_directory_uri() . '/js/lazysizes.min.js', false, '1.0', true );
}

// Remove wp version param from any enqueued scripts
function macleods_furniture_remove_wp_ver( $src ) {
   	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
    	$src = remove_query_arg( 'ver', $src );
   	return $src;
}
add_filter( 'style_loader_src', 'macleods_furniture_remove_wp_ver', 9999 );
add_filter( 'script_loader_src', 'macleods_furniture_remove_wp_ver', 9999 );

function macleods_furniture_custom_footer() {
	ob_start();
	?>
		<script type="text/javascript">
			function downloadJSAtOnload() {
				var element = document.createElement("script");
				element.src = "<?php echo get_stylesheet_directory_uri(); ?>/js/defer.js";
				document.body.appendChild(element);
			}
			
			if (window.addEventListener)
				window.addEventListener("load", downloadJSAtOnload, false);
			else if (window.attachEvent)
				window.attachEvent("onload", downloadJSAtOnload);
			else window.onload = downloadJSAtOnload;
		</script>
	<?php
	echo ob_get_clean();
}
add_action( 'wp_footer', 'macleods_furniture_custom_footer' );

/**********************************
 * Customize WP Login
 **********************************/
function macleods_furniture_login_logo() 
{
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo 			= wp_get_attachment_image_src( $custom_logo_id , 'full' );
	?>
	<style type="text/css">
		.login h1 a {
			background: url('<?php echo esc_url( $logo[0] ) ?>') top center no-repeat;
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500" rel="stylesheet">
	<?php 
}
add_action( 'login_head', 'macleods_furniture_login_logo' );

function macleods_furniture_login_stylesheet() {
    wp_enqueue_style( 'rstheme-custom-login-css', get_stylesheet_directory_uri() . '/css/login-form.css' );
    wp_enqueue_script( 'rstheme-custom-login-js', get_stylesheet_directory_uri() . '/js/login-form.js' );
}
add_action( 'login_enqueue_scripts', 'macleods_furniture_login_stylesheet' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}