<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package macleods-furniture
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
function macleods_furniture_woocommerce_setup() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'macleods_furniture_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function macleods_furniture_woocommerce_scripts() {
	
	$woo_css_ver = date("ymd-Gis", filemtime( get_template_directory() . '/css/woocommerce.css' ));
	wp_enqueue_style( 'macleods_furniture_woocommerce-style', get_template_directory_uri() . '/css/woocommerce.css', false, $woo_css_ver );
	
	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@-webkit-keyframes spin{
			100%{
				-webkit-transform:rotate(360deg);
				transform:rotate(360deg)
			}
		}
		@keyframes spin{
			100%{
				-webkit-transform:rotate(360deg);
				transform:rotate(360deg)
			}
		}
		@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}
		@font-face {
			font-family: "WooCommerce";
			src:url("' . $font_path . 'WooCommerce.eot");
			src:url("' . $font_path . 'WooCommerce.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'WooCommerce.woff") format("woff"),
				url("' . $font_path . 'WooCommerce.ttf") format("truetype"),
				url("' . $font_path . 'WooCommerce.svg#WooCommerce") format("svg");
			font-weight: 400;
			font-style: normal;
		}
		';

	wp_add_inline_style( 'macleods_furniture_woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'macleods_furniture_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function macleods_furniture_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	if( is_shop() )
		$classes[] = 'woocommerce-shop';
	
	if( is_product_category() )
		$classes[] = 'woocommerce-product-category';

	return $classes;
}
add_filter( 'body_class', 'macleods_furniture_woocommerce_active_body_class' );

/**
 * Product gallery thumnbail columns._
 *
 * @return integer number of columns.
 */
function macleods_furniture_woocommerce_thumbnail_columns() {
	return 4;
}
add_filter( 'woocommerce_product_thumbnails_columns', 'macleods_furniture_woocommerce_thumbnail_columns' );

/**
 * Change a currency symbol
 */
function macleods_furniture_change_existing_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'AUD': $currency_symbol = 'AUD $'; break;
	}
	
	return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'macleods_furniture_change_existing_currency_symbol', 10, 2);


/**
 * Default loop columns on product archives.
 *
 * @return integer products per row.
 */
function macleods_furniture_woocommerce_loop_columns() {
	return 3;
}
add_filter( 'loop_shop_columns', 'macleods_furniture_woocommerce_loop_columns' );

// Remove add to cart button
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

function macleods_furniture_woocommerce_after_shop_loop_item_cb() {
	global $product;

	if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
		?>
			<div class="stock out-of-stock"><p class="mb-0">Out of stock</p></div>
		<?php
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'macleods_furniture_woocommerce_after_shop_loop_item_cb', 30 );

/**
 * Filter sale display
 */
function macleods_furniture_woocommerce_sale_flash_cb( $post, $product ) {
	return '<span class="onsale">' . esc_html__( 'Sale', 'woocommerce' ) . '</span>';
}
add_filter( 'woocommerce_sale_flash', 'macleods_furniture_woocommerce_sale_flash_cb', 10, 2 );


/**
 * Related Products.
 */

 // remove builtin related-product
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/*
 * Filter the number of columns and number of posts to display
 * 
 * @param array $args related products args.
 * @return array $args related products args.
 */
function macleods_furniture_woocommerce_related_products_args( $args ) {
	$defaults = [
		'posts_per_page' => 4,
		'columns'        => 4,
	];

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'macleods_furniture_woocommerce_related_products_args' );

// Remove default WooCommerce wrapper.
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'macleods_furniture_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function macleods_furniture_woocommerce_wrapper_before() {
		?>
		<div id="primary" class="content-area">
			
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
			
						<main id="main" class="site-main" role="main">
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'macleods_furniture_woocommerce_wrapper_before' );

if ( ! function_exists( 'macleods_furniture_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function macleods_furniture_woocommerce_wrapper_after() {
						?>
						</main><!-- #main -->

					</div> <!-- .col-lg-12 -->
				</div> <!-- .row -->
			</div> <!-- .container -->

		</div><!-- #primary -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'macleods_furniture_woocommerce_wrapper_after' );

/**
 * Checkout Page
 */
function macleods_furniture_order_review_strings( $translated_text, $text, $domain ) {

	if( is_checkout() ) {
		switch ( $translated_text ) {
			case 'Billing details' :
				$translated_text = __( 'Billing & Shipping Information', 'woocommerce' );
				break;
		}
	}

	if( is_cart() ) {
		switch ( $translated_text ) {
			case 'Cart totals' :
				$translated_text = __( 'Order Summary', 'woocommerce' );
				break;
		}
	}
	
	return $translated_text;
}
add_filter( 'gettext', 'macleods_furniture_order_review_strings', 20, 3 );

function macleods_furniture_custom_button_text( $button_text ) {
	return 'Process Order';
}
add_filter( 'woocommerce_order_button_text', 'macleods_furniture_custom_button_text' );


/**
 * Cart Page
 */


/**
 * Archive product page
 */
add_filter('woocommerce_show_page_title', '__return_false');
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20, 0 );				// Remove result count

// filter shop title
function macleods_furniture_shop_page_title( $page_title ) {
	if( 'Shop' == $page_title ) {
		return __('All Products', 'woocommerce');
	}

	return $page_title;
}
add_filter( 'woocommerce_page_title', 'macleods_furniture_shop_page_title');

// start product archive
add_action( 'woocommerce_before_shop_loop', function(){
	?>
		<div class="row">
	<?php
}, 11 );

	// start product archive sidebar
	add_action( 'woocommerce_before_shop_loop', function(){
		?>
			<aside class="offset-lg-2 col-lg-2">
				<?php dynamic_sidebar( 'shop' ); ?>
		<?php
	}, 12 );
	
	// end product archive sidebar
	add_action( 'woocommerce_before_shop_loop', function(){
		?>
			</aside>
		<?php
	}, 14 );

	// return the number of product to show per page
	add_filter( 'loop_shop_per_page', function( $products ) {
		$shop_per_page = isset($_GET['shop_per_page']) ? $_GET['shop_per_page'] : 12;
		
		return $shop_per_page;
	}, 20 );

	// start product archive main content
	add_action( 'woocommerce_before_shop_loop', function(){
		?>
			<main class="col-lg-6">
		<?php
	}, 16 );
		
		// start filter-wrap
		add_action( 'woocommerce_before_shop_loop', function(){
			?>
			<div class="mb-8 flex flex-wrap align-items-center justify-content-between product-display-filter">
				<div class="flex">
					<a class="mr-2" href="javascript: void(0);" data-id="grid">
						<svg width="24" height="24" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
							<rect width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="24" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="48" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect y="48" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="24" y="48" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="48" y="48" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect y="24" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="24" y="24" width="16" height="16" rx="3" fill="#a5a4a4"/>
							<rect x="48" y="24" width="16" height="16" rx="3" fill="#a5a4a4"/>
						</svg>
					</a>
					<a href="javascript: void(0);" data-id="list">
						<svg width="24" height="24" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
							<rect width="64" height="12" rx="6" fill="#a5a4a4"/>
							<rect y="52" width="64" height="12" rx="6" fill="#a5a4a4"/>
							<rect y="26" width="64" height="12" rx="6" fill="#a5a4a4"/>
						</svg>
					</a>
				</div>
			<?php
		}, 18 );

			// start posts-per-page filter
			add_action( 'woocommerce_before_shop_loop', function(){
				?>
				<div class="flex align-items-center gap-y-4">
					<?php
						$shop_per_page = isset($_GET['shop_per_page']) ? $_GET['shop_per_page'] : 12;
					?>
					
					<form method="GET" onchange="this.submit();">
						<div class="flex align-items-center">
							<span class="mr-2 text-gray-60">Show</span>
							<select name="shop_per_page" id="shop_per_page" 
								class="pl-3 pr-8"
								aria-label="Shop product per page"
								style="background-position-x: 80%;">
								<?php
									foreach( range(3, 24, 3) as $option ) {
										?>
											<option value="<?php echo $option; ?>" <?php selected( $shop_per_page, $option ); ?>>
												<?php echo $option; ?>
											</option>
										<?php	
									}
								?>
							</select>
						</div>
					</form>
				
				<?php
			}, 28 );

			// end posts-per-page filter
			add_action( 'woocommerce_before_shop_loop', function(){
				?>
					</div>
				<?php
			}, 32 );

		// end filter-wrap
		add_action( 'woocommerce_before_shop_loop', function(){
			?>
				</div>
			<?php
		}, 34 );
		
	// end product archive main content
	add_action( 'woocommerce_after_shop_loop', function(){
		?>
			</main>
		<?php
	}, 20 );

// end product archive
add_action( 'woocommerce_after_shop_loop', function(){
	?>
		</div>
	<?php
}, 99 );

// open products wrap
add_action('woocommerce_before_shop_loop_item_title', function(){
	?>
		<div>
	<?php
}, 12);

// close products wrap
add_action('woocommerce_after_shop_loop_item_title', function(){
	?>
		</div>
	<?php
}, 12);

/**
 * Single product page
 */

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15, 0 );	// Remove upsell
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );								// Remove sidebar
add_filter( 'wc_product_sku_enabled', '__return_false' );											// Remove product SKU

function macleods_furniture_woocommerce_after_single_product_summary_before() {
	?>
	<div class="row">
		<div class="offset-lg-2 col-lg-8">
	<?php
}
add_action( 'woocommerce_after_single_product_summary', 'macleods_furniture_woocommerce_after_single_product_summary_before', 8 );

function macleods_furniture_woocommerce_after_single_product_summary_after() {
	?>
		</div> <!--.col-lg-6-->
	</div> <!--.row-->
	<?php
}
add_action( 'woocommerce_after_single_product_summary', 'macleods_furniture_woocommerce_after_single_product_summary_after', 12 );

// add single product custom breadcrumb
add_action( 'woocommerce_single_product_summary', function() {
	$terms = get_the_terms( get_the_ID(), 'product_cat');
	
	?>
	<div class="mb-8 breadcrumbs">
		<span class="text-gray-70">Home</span>

		<?php
			if ( $terms && ! is_wp_error( $terms ) ) {
				foreach( $terms as $term ) {
					?>
					<span class="px-2 text-gray-10">/</span>
					<a class="text-blue" href="<?php echo esc_url( get_category_link( $term ) ); ?>" title="<?php echo esc_attr($term->name); ?>">
						<?php echo $term->name; ?>
					</a>
					<?php
				}
			}
		?>
	</div>
	<?php
}, 2);

// add social media share
add_action( 'woocommerce_single_product_summary', function() {
	if( ! is_front_page() ) {
		?>
			<div class="mt-2 flex align-items-center">
				<p class="mr-2 mb-0">Share:</p>
				<div class="flex align-items-center">
					<a class="mr-4" href="https://www.facebook.com/share.php?u=<?php the_permalink(); ?>" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-icon.png" alt="Facebook">
					</a>
					<a href="https://www.instagram.com/?url=<?php bloginfo('url'); ?>" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ig-icon.png" alt="Facebook">
					</a>
				</div>
			</div>
		<?php
	}
}, 42 );

function macleods_furniture_woocommerce_after_single_product_func() {}
add_action( 'woocommerce_after_single_product', 'macleods_furniture_woocommerce_after_single_product_func' );

function macleods_furniture_product_carousel_options( $options ) {
	/**
	 * array( 
	 * 	'rtl' 			=> is_rtl(),
	 * 	'animation' 	=> 'slide',
	 * 	'smoothHeight' 	=> false,
	 * 	'directionNav' 	=> false,
	 * 	'controlNav' 	=> 'thumbnails',
	 * 	'slideshow' 	=> false,
	 * 	'animationSpeed' => 500,
	 * 	'animationLoop' => false
	 * )
	 */
	
	$options = [
		'animation' 	=> 'fade',
		'controlNav' 	=> 'thumbnails',
		'directionNav' 	=> true,
		'slideshow'		=> true,
		'allowOneSlide'	=> true
	];
	
	return $options;
}
add_filter( "woocommerce_single_product_carousel_options", "macleods_furniture_product_carousel_options", 10, 1);

// Make gallery image size 
add_filter( 'woocommerce_gallery_image_size', function( $size ) { return [740,800]; } );

function macleods_furniture_change_option_none_text( $args ) {
	// Only change text for these two product attributes.
	if ( 'size' == strtolower( $args['attribute'] )
		|| 'pa_size' == $args['attribute']
		|| 'attribute_size' == $args['attribute'] ) {
		$args['show_option_none'] = 'Select';
	}
	
	return $args;
}
// add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'macleods_furniture_change_option_none_text' );

function macleods_furniture_woocommerce_short_description_func( $post_excerpt ) {
	if( is_product() ) {
		if( empty( $post_excerpt ) || "" == $post_excerpt ) {
			return wpautop( get_the_content() );
		}
	}
	
	return $post_excerpt;
}
add_filter( 'woocommerce_short_description', 'macleods_furniture_woocommerce_short_description_func', 99 );

// change the breadcrumb default behavior
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter', 20 );
function wcc_change_breadcrumb_delimiter( $defaults ) {
	$defaults['home'] = _x( '<span class="text-gray-70">Home</span>', 'breadcrumb', THEME_DOMAIN );
	$defaults['delimiter'] = '<span class="px-2 text-gray-10">/</span>';

	return $defaults;
}

// start add row tag before shop/archive breadcrumb
add_action('woocommerce_before_main_content', function(){
	?>
		<div class="row breadcrumb-wrap">
			<div class="offset-lg-2 col-lg-8">	
				
				<div class="mb-12 py-4 border-b border-gray-5">
	<?php
}, 18);
	
	// remove the breadcrumbs except archive page 
	add_action( 'template_redirect', 'macleods_furniture_remove_wc_breadcrumbs' );
	function macleods_furniture_remove_wc_breadcrumbs() {
		if( ! (is_product_category() || is_shop()) ) {
			remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
		}
	}

// end add row tag before shop/archive breadcrumb
add_action('woocommerce_before_main_content', function(){
	?>
				</div>

			</div>
		</div>
	<?php
}, 22);

add_action('woocommerce_before_add_to_cart_quantity', function(){
	?>
	<div class="flex justify-content-between">
		<div class="flex align-items-center quantity-wrapper">
			<div class="quantity-label">Qty</div>
			<div class="quantity-btn-wrapper">
	<?php
}, 2);

	add_action('woocommerce_after_add_to_cart_quantity', function(){
		?>
			<div class="quantity-btn">
				<button type="button" class="qty_button plus">+</button>
				<button type="button" class="qty_button minus">-</button>
			</div>
		</div> <!--.quantity-btn-wrapper-->
			
		</div> <!--flex align-items-center quantity-wrapper-->
		<?php
	}, 2);

add_action('woocommerce_after_add_to_cart_button', function(){
	?>
	</div> <!--.flex-->
	<?php
}, 2);


/**
 * Show cart contents / total Ajax
 */
function macleods_furniture_header_add_to_cart_fragment( $fragments ) {
	ob_start();

	?>
		<a class="ml-4 block relative cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'macleods-furniture'); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cart-icon.png" alt="Cart">
			<span class="absolute text-xs bg-gray flex justify-content-center align-items-center text-white rounded-3xl cart-count"><?php echo sprintf(_n('%d', '%d', WC()->cart->cart_contents_count, 'macleods-furniture'), WC()->cart->cart_contents_count);?></span>
		</a>
	<?php
	
	$fragments['a.cart-customlocation'] = ob_get_clean();

	return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'macleods_furniture_header_add_to_cart_fragment' );

/**
 * Third party extension
 */
function custom_woocommerce_shipment_tracking_default_provider( $provider ) {
	$provider = 'Australia Post'; 
	
	return $provider;
}
add_filter( 'woocommerce_shipment_tracking_default_provider', 'custom_woocommerce_shipment_tracking_default_provider' );

/**
 * This function loops over cart items, and moves any item with shipping class 'special-class' into a new package. 
 * The new package in this example only takes flat rate shipping.
 * https://gist.github.com/mikejolley/347b8f162257f6736c4d
 */
function macleods_furniture_split_special_shipping_class_items( $packages ) {
	$found_item                     = false;
	$special_class                  = 'local-pickup'; // edit this with the slug of your shippig class
	$new_package                    = current( $packages );
	$new_package['contents']        = array();
	$new_package['contents_cost']   = 0;
	$new_package['applied_coupons'] = array();
	$new_package['ship_via']        = array( 'local_pickup' ); // Only allow flat rate for items in special class

	//bit of a hack. Displaying "LOCAL PICK UP ONLY" as the shipping package name is confusing if there are no shipping methods available.
	if( WC()->customer->get_shipping_state() != 'VIC'){
		$new_package['name'] = 'No Shipping Available';
	} else {
		$new_package['name'] = 'Local Pickup Only';  //custom label for this package
	}

	foreach ( WC()->cart->get_cart() as $item_key => $item ) {
		// Is the product in the special class?
		if ( $item['data']->needs_shipping() && $special_class === $item['data']->get_shipping_class() ) {
			$found_item                            = true;
			$new_package['contents'][ $item_key ]  = $item;
			$new_package['contents_cost']         += $item['line_total'];

			// Remove from original package
			$packages[0]['contents_cost'] = $packages[0]['contents_cost'] - $item['line_total'];
			unset( $packages[0]['contents'][ $item_key ] );

			// If there are no items left in the previous package, remove it completely.
			if ( empty( $packages[0]['contents'] ) ) {
				unset( $packages[0] );
			}
		}
	}
	if ( $found_item ) {
	   $packages[] = $new_package;
	}
	return $packages;
}
add_filter( 'woocommerce_cart_shipping_packages', 'macleods_furniture_split_special_shipping_class_items' );

/**
 * change the checkout error message from "No shipping options were found for %s."
 */
function macleods_furniture_change_no_shipping_text ($text){
	$text = str_replace('There are no shipping options available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'This item does not ship to your location. It is local pick up only.', $text);
	return $text;

}
add_filter( 'woocommerce_no_shipping_available_html', 'macleods_furniture_change_no_shipping_text' );

/** 
 * Change the name of the local shipping package from "Shipping 2" to the name set above ($new_package['name'])
 */
function macleods_furniture_rename_custom_package( $package_name, $i, $package ) {
	if ( ! empty( $package['name'] ) ) {
		$package_name = $package['name'];
	}

	return $package_name;
}
add_filter( 'woocommerce_shipping_package_name', 'macleods_furniture_rename_custom_package', 10, 3 );

/**
 * Change the cart error message from "No shipping options were found for %s."
 */
function macleods_furniture_change_cart_no_shipping_text ($text){
	
	$text = str_replace('No shipping options were found for', 'This item does not ship to', $text);
	return $text . 'It is local pick up only.';

}
add_filter( 'woocommerce_cart_no_shipping_available_html', 'macleods_furniture_change_cart_no_shipping_text' );

/**
 * Set Wishlist counter
 */
function macleods_furniture_yith_wcwl_ajax_update_count(){
	wp_send_json( array(
		'count' => yith_wcwl_count_products(),
	) );
}
add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'macleods_furniture_yith_wcwl_ajax_update_count' );
add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'macleods_furniture_yith_wcwl_ajax_update_count' );

if ( defined( 'YITH_WCWL' ) && ! function_exists( 'macleods_furniture_yith_wcwl_enqueue_custom_script' ) ) {
	function macleods_furniture_yith_wcwl_enqueue_custom_script() {
	  wp_add_inline_script(
		'jquery-yith-wcwl',
		"
		  jQuery( function( $ ) {
			$( document ).on( 'added_to_wishlist removed_from_wishlist', function() {
			  $.get( yith_wcwl_l10n.ajax_url, {
					action: 'yith_wcwl_update_wishlist_count'
			  }, function( data ) {
					$('.wishlist-count').html( data.count );
			  } );
			} );
		} );
		"
	  );
	}
	add_action( 'wp_enqueue_scripts', 'macleods_furniture_yith_wcwl_enqueue_custom_script', 20 );
}

/**
 * Add custom sorting options (asc/desc)
 */
function macleods_furniture_custom_woocommerce_get_catalog_ordering_args( $args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	 
	if( 'alphabetical' == $orderby_value ) {
		$args['orderby'] 	= 'title';
		$args['order'] 		= 'ASC';
		$args['meta_key'] 	= '';
	}

	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'macleods_furniture_custom_woocommerce_get_catalog_ordering_args' );

function macleods_furniture_custom_woocommerce_catalog_orderby( $sortby ) {
	$sortby['alphabetical'] = 'Sort by alphabetical A to Z';
	
	return $sortby;
}
add_filter( 'woocommerce_default_catalog_orderby_options', 'macleods_furniture_custom_woocommerce_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'macleods_furniture_custom_woocommerce_catalog_orderby' );

// convert color attribute from dropdown to radio
function variation_radio_buttons($html, $args) {
	
  $args = wp_parse_args(apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args), array(
    'options'          => false,
    'attribute'        => false,
    'product'          => false,
    'selected'         => false,
    'name'             => '',
    'id'               => '',
    'class'            => '',
    'show_option_none' => __('Choose an option', 'woocommerce'),
  ));

  if(false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product) {
    $selected_key     = 'attribute_'.sanitize_title($args['attribute']);
    $args['selected'] = isset($_REQUEST[$selected_key]) ? wc_clean(wp_unslash($_REQUEST[$selected_key])) : $args['product']->get_variation_default_attribute($args['attribute']);
  }

  $options               = $args['options'];
  $product               = $args['product'];
  $attribute             = $args['attribute'];
  $name                  = $args['name'] ? $args['name'] : 'attribute_'.sanitize_title($attribute);
  $id                    = $args['id'] ? $args['id'] : sanitize_title($attribute);
  $class                 = $args['class'];
  $show_option_none      = (bool)$args['show_option_none'];
  $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce');

  if(empty($options) && !empty($product) && !empty($attribute)) {
    $attributes = $product->get_variation_attributes();
    $options    = $attributes[$attribute];
  }	

	$radios = '<div class="variation-radios">';

		if( ! empty( $options ) ) {
			if($product && taxonomy_exists($attribute)) {
				$terms = wc_get_product_terms($product->get_id(), $attribute, array(
					'fields' => 'all',
				));
				
				foreach( $terms as $term ) {
					if(in_array($term->slug, $options, true)) {
						$id = $name.'-'.$term->slug;
						$thumbnail_url = get_field('field_61b5ef3cbba03', $term);
						
						$radios .= '
							<label for="'.esc_attr($id).'">	
								<input type="radio" id="'.esc_attr($id).'" name="'.esc_attr($name).'" value="'.esc_attr($term->slug).'" '.checked(sanitize_title($args['selected']), $term->slug, false).'>
								<span class="hidden">'. esc_html(apply_filters('woocommerce_variation_option_name', $term->name)) .'</span>
								<img src="'. esc_url($thumbnail_url['url']) .'" alt="" />
							</label>';
					}
				}
			} 
			else {
				foreach($options as $option) {
					$id = $name.'-'.$option;
					$thumbnail_url = get_field('field_61b5ef3cbba03', $term);
					
					$checked    = sanitize_title($args['selected']) === $args['selected'] ? checked($args['selected'], sanitize_title($option), false) : checked($args['selected'], $option, false);
					$radios    .= '
						<label for="'.esc_attr($id).'">
							<input type="radio" id="'.esc_attr($id).'" name="'.esc_attr($name).'" value="'.esc_attr($option).'" id="'.sanitize_title($option).'" '.$checked.'>
							<span class="hidden">'. esc_html(apply_filters('woocommerce_variation_option_name', $option)) .'</span>
							<img src="'. esc_url($thumbnail_url['url']) .'" alt="" />
						</label>';
				}
			}
		}

	$radios .= '</div>';
    
	if( 'pa_color' == $attribute ) {
		return $html . $radios;
	}

  return $html;
}
add_filter('woocommerce_dropdown_variation_attribute_options_html', 'variation_radio_buttons', 20, 2);

function variation_check($active, $variation) {
  if(!$variation->is_in_stock() && !$variation->backorders_allowed()) {
    return false;
  }
  return $active;
}
add_filter('woocommerce_variation_is_active', 'variation_check', 10, 2);

add_action( 'wp_head', function() {
	// Only add brand markup on products
	if ( ! is_singular( 'product' ) ) {
		return;
	}
	
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	
	// get product
	$product = wc_get_product( get_the_ID() );
	
	// Get product price
	$price = ( $product ) ? $product->get_price() : false;
	$availability = ( $product->is_in_stock() ) ? 'http://schema.org/InStock' : 'https://schema.org/OutOfStock';
	?>
	<script type="application/ld+json">
		{
		  "@context": "http://schema.org/",
		  "@type": "Product",
		  "name": "<?php echo get_bloginfo('name'); ?>",
		  "image": "<?php echo $logo[0]; ?>",
		  "description": "From the seeds of an idea we create a thing of beauty",
		  "brand": {
		    "@type": "Thing",
		    "name": "<?php echo get_bloginfo('name'); ?>"
		  },
		  "offers": {
		    "@type": "Offer",
		    "priceCurrency": "AUD",
		    "price": "<?php echo $price; ?>",
        "priceValidUntil": "<?php echo date('Y-m-d'); ?>",
        "url": "<?php echo get_the_permalink(); ?>",
        "availability": "<?php echo $availability; ?>"
		  }
		}
	</script>
	<?php 
} );