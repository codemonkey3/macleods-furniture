<?php

// this dump viriable and wrap with the pre tag
function _var_dump( $variable = '' ) {
  echo '<pre>';
    var_dump( $variable );
  echo '</pre>';
}

function mf_chosen_attributes() {
	$chosen_attributes = array();
	$query_string = $_SERVER['QUERY_STRING'];
	
	if( $query_string ) {
		// get filters
		$query_string_items = explode( '&', $query_string );
		foreach( $query_string_items as $query_string_item ) {
			$terms = explode( '=', $query_string_item );
			
			// get original name or taxonomy name
			$taxonomy = sanitize_title( str_replace( 'filter_', 'pa_', $terms[0] ) );
			
			// get taxonomy value
			$term_value = end( $terms );
			$term_value_item = explode( ',', $term_value );
			
			$chosen_attributes[$taxonomy] = array(
				'terms' => $term_value_item
			);
		}
	}
	
	return $chosen_attributes;
}


function _get_post_ids( $post_type = 'post', $order = 'DESC' ) {

  // make sure the post-type exist
  if( ! post_type_exists( $post_type ) ) 
    return;
  
  $args = [
    'post_type'     => $post_type,
    'numberposts'   => 1,
    'order'         => $order,
    'fields'        => 'ids' // returned only the ID
  ];

  // get post_ids
  $post_ids = get_posts( $args )[0];

  // rest postdata
  wp_reset_postdata();
  
  return $post_ids;
}

/**
 * Get latest article post-type ID
 */
function get_latest_article_id() {
  return _get_post_ids( 'post' );
}

/**
 * Get oldest article post-type ID
 */
function get_oldest_article_id() {
  return _get_post_ids( 'post', 'ASC' );
}

/**
 * Get next article post-id
 */
function macleods_furniture_get_next_article_id() {
  if( is_single()
    && 'post' == get_post_type() ) {
    
    // get next post
    $next_post = get_next_post();
    
    if ( is_a( $next_post , 'WP_Post' ) ) {
      $loading_url    = get_permalink( $next_post->ID );
      $loading_title  = get_the_title( $next_post->ID );

      // set next latest-project ID
      $next_item_id    = $next_post->ID;
    } else {
      $loading_url        = get_permalink( get_oldest_article_id() );
      $loading_title      = get_the_title( get_oldest_article_id() );

      // set next latest-project ID
      $next_item_id       = get_oldest_article_id();
    }
    
    return $next_item_id;
  }

  return false;
}