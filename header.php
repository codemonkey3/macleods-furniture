<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package macleods-furniture
 */

$options = get_option( 'rs_theme_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}

// get ACF value
$login_page = get_field('field_61ac19d6e1c0e', 'options');
?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'macleods-furniture' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="mb-9 site-header" role="banner" style="<?php echo $masthead_style; ?>">
		
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						
						<div class="text-xs">
							<?php
								// Check rows exists.
								if( have_rows('field_61a8c20e3e04a', 'options') ) {
									?>
									<div class="flex align-items-center">
										<?php
											$item_counter = 1;
											$item_count = count( get_field('field_61a8c20e3e04a', 'options') );
											
											// Loop through rows.
											while( have_rows('field_61a8c20e3e04a', 'options') ) { the_row();

												// Load sub field value.
												$contact = get_sub_field('field_61a8c303d5147');
												?>
													<div class="<?php echo ($item_counter < $item_count) ? 'mr-1' : ''; ?>">
														<?php echo $contact; ?>
													</div>
												<?php

												// increase our counter
												$item_counter++;
											}
										?>
									</div>
									<?php
								}
							?>
						</div>

					</div>
					<div class="col-lg-7">
						<?php
							// Check rows exists.
							if( have_rows('field_61a8c2403e04c', 'options') ) {
								?>
								<ul class="mb-0 ml-0 pl-0 flex align-items-center justify-content-center justify-content-lg-end list-none">
									<?php
										// Loop through rows.
										while( have_rows('field_61a8c2403e04c', 'options') ) { the_row();

											// Load sub field value.
											$icon = get_sub_field('field_61a8c27a3e04e');
											$link = get_sub_field('field_61a8c27f3e04f');
											?>
												<li class="relative">
													<a class="text-xs text-uppercase block" href="<?php echo esc_url( $link['url'] ); ?>" title="<?php echo esc_attr($link['title']); ?>"														>
														<div class="flex align-items-center">
															<img class="mr-2" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr( $icon['alt'] ); ?>">
															<span><?php echo esc_attr( $link['title'] ); ?></span>
														</div>
													</a>
												</li>
											<?php

										}

										if( is_user_logged_in() ) {
											?>
											<li class="relative logged-out">
												<a class="text-xs text-uppercase block" href="<?php echo wp_logout_url( home_url() ); ?>">
													<div class="flex align-items-center">
														<img class="mr-2" src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/images/login-icon.png" alt="<?php echo esc_attr( $icon['alt'] ); ?>">
														<span>Log Out</span>
													</div>
												</a>
											</li>
											<?php
										}
										else {
											?>
											<li class="relative logged-in">
												<a class="text-xs text-uppercase block" href="<?php echo esc_url( $login_page['url'] ); ?>">
													<div class="flex align-items-center">
														<img class="mr-2" src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/images/login-icon.png" alt="<?php echo esc_attr( $icon['alt'] ); ?>">
														<span>Log In</span>
													</div>
												</a>
											</li>
											<?php
										}
									?>
								</ul>
								<?php
							}
						?>
					</div>
				</div>

			</div> <!--container-->
		</div> <!--header-top-->

		<div class="header-bottom">
			<div class="container">

				<div class="row align-items-center justify-content-between">
					<div class="col-lg-6">
						
						<div class="media align-items-center">
							<div class="site-branding">
								<?php
								$custom_logo_id = get_theme_mod( 'custom_logo' );
								$logo 			= wp_get_attachment_image_src( $custom_logo_id , 'full' );

								if ( is_front_page() && is_home() ) {
									?>
									<h1 class="site-title mb-0">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<?php if( has_custom_logo() ) { ?>

												<img class="d-block" src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php get_bloginfo('name') ?>" />

											<?php } else { ?>

												<?php bloginfo( 'name' ); ?>
												
											<?php } ?>
										</a>
									</h1>
									<?php

								}
								else {

									?>
									<p class="site-title mb-0">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<?php if( has_custom_logo() ) { ?>

												<img src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php bloginfo('name') ?>" />

											<?php } else { ?>

												<img class="d-block" src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php get_bloginfo('name') ?>" />

											<?php } ?>
										</a>
									</p>
									<?php 
								}

								$description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) {
									?>
										<p class="hidden site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
									<?php
								}
								?>
							</div><!-- .site-branding -->
							<div class="partners">
								<?php
									// Check rows exists.
									if( have_rows('field_61a88b108d22f', 'options') ) {
										?>
										<div class="flex align-items-center">
											<?php
												// Loop through rows.
												while( have_rows('field_61a88b108d22f', 'options') ) { the_row();

													// Load sub field value.
													$logo = get_sub_field('field_61a88b1d8d230');
													?>
														<img src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php echo esc_url( $logo['alt'] ); ?>">
													<?php

												}
											?>
										</div>
										<?php
									}
								?>
							</div>
						</div>

					</div>
					<div class="col-lg-3">
						<div class="flex justify-content-center justify-content-lg-end align-items-center">
							<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
								<label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
								<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search here...', 'placeholder', 'macleods-furniture' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'macleods-furniture' ); ?>" />
								<input type="hidden" name="post_type" value="product" />
							</form>
							
							<a class="ml-4 block relative cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'macleods-furniture'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cart-icon.png" alt="Cart">
								<span class="absolute text-xs bg-gray flex justify-content-center align-items-center text-white rounded-3xl cart-count"><?php echo sprintf(_n('%d', '%d', WC()->cart->cart_contents_count, 'macleods-furniture'), WC()->cart->cart_contents_count);?></span>
							</a>
							
							<button class="ml-6 menu-toggle" aria-controls="primary-menu" aria-expanded="false">
								<span></span>
								<span></span>
								<span></span>
							</button>
						</div>
					</div>
				</div>
			</div> <!--container-->
		</div>
	</header><!-- #masthead -->

	<nav class="main-navigation" role="navigation">
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12">

					<?php
						// Check rows exists.
						if( have_rows('field_61ac0b52d7265', 'options') ) {
							?>
							<ul class="mb-0 ml-0 pl-0 flex justify-content-between list-none">
								<?php
									// Loop through rows.
									while( have_rows('field_61ac0b52d7265', 'options') ) { the_row();

										// Load sub field value.
										$icon = get_sub_field('field_61ac0b52d7266');
										$term_id = get_sub_field('field_61b0a1c27c1aa');
										
										$term = get_term( $term_id, 'product_cat' );
										?>
											<li>
												<a class="py-6 hover:bg-blue text-gray-90 hover:text-white block" href="<?php echo esc_url( get_category_link($term_id) ); ?>" title="<?php echo esc_attr( $term->name ); ?>">
													<div class="text-center">
														<img class="mb-2" src="<?php echo esc_url( $icon['url'] ); ?>" alt="<?php echo esc_url( $icon['alt'] ); ?>">
														<span class="font-bold block text-uppercase"><?php echo $term->name; ?></span>
													</div>
												</a>
											</li>
										<?php

									}
								?>
							</ul>
							<?php
						}
					?>

				</div>
			</div>
		</div>

	</nav> <!--.main-navigation-->
	
	<div id="content" class="site-content">

	<?php
		if( is_front_page() ) {
			
			?>
			<div class="hero-slider">

				<?php
					// Check rows exists.
					if( have_rows('field_61a8c6d6b8e07') ) {
				
						// Loop through rows.
						while( have_rows('field_61a8c6d6b8e07') ) { the_row();

							// Load sub field value.
							$slider_bg_color = get_sub_field('field_61b2cee552c61');
							$slider_hero = get_sub_field('field_61a8c6fcb8e0a');
							$heading = get_sub_field('field_61a8c6472cd5b');
							$excerpt = get_sub_field('field_61a8c65a2cd5c');
							?>
								<div>
									<div class="font-light text-white item"
									style="background-color:<?php echo $slider_bg_color ? $slider_bg_color : '#fff'; ?>; background-image: url(<?php echo esc_url( $slider_hero['url'] ); ?>);">
										<div class="container">
											<div class="row">
												<div class="offset-lg-1 col-lg-5">
													
													<p class="text-2xl">Featured Products</p>
													
													<h2 class="mb-8 font-caflisch">
														<?php echo $heading; ?>
													</h2>
													<article class="text-xl">
														<?php echo wpautop( $excerpt ); ?>
													</article>

													<?php
														// Check rows exists.
														if( have_rows('field_61a8c6712cd5d') ) {
															?>
															<div class="flex align-items-center">
																<?php
																	// Loop through rows.
																	while( have_rows('field_61a8c6712cd5d') ) { the_row();
																		$btn_ui = get_sub_field('field_61a8c68c2cd5e');
																		$link = get_sub_field('field_61a8c6aa2cd5f');
																		?>
																			<a class="lg:mr-3 btn btn-<?php echo $btn_ui; ?>" href="<?php echo esc_url($link['url']); ?>">
																				<?php echo $link['title']; ?>
																			</a>
																		<?php
																	}
																?>
															</div>
															<?php
														}
													?>

												</div>
											</div>
										</div>
									</div>
								</div>
							<?php

						}
							
					}
				?>
				
			</div>
			<?php
		}
	?>