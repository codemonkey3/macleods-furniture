<?php
/**
 * Template Name: Full Width
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package macleods-furniture
 */

get_header();

// hero
$hero_image = get_field('field_61a88f4ee5f18');
$hero_title = get_field('field_61a88fb0e5f19');
?>

	<div class="mb-12 py-21 hero"
		style="background-image: url(<?php echo $hero_image['url']; ?>)">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					
					<header class="text-center text-white">
						<h1 class="mb-0 font-caflisch font-semibold text-6xl">
							<?php echo $hero_title ? $hero_title : get_the_title(); ?>
						</h1>
					</header>
					<div class="font-caflisch breadcrumbs text-2xl">
						<?php echo get_template_part('inc/breadcrumb'); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="container">
				<div class="row">
					<div class="offset-lg-2 col-lg-8">

						<div class="entry-content">
							<?php
								the_content();
								
								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'macleods-furniture' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->

					</div>
				</div>
			</div> <!-- .container -->

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();
