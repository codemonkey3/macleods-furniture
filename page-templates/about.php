<?php
/**
 * Template Name: About Us
 * 
 * The template for displaying about page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package macleods-furniture
 */

get_header();

// get ACF value
// hero
$hero_image = get_field('field_61a88f4ee5f18');
$hero_title = get_field('field_61a88fb0e5f19');

// who we are
$who_we_are = get_field('field_61a88fe04d212');
$who_we_are_image = get_field('field_61a8909f4d214');

// services
$services_bg_color = get_field('field_61acc12aace8e');
?>
	
	<div class="mb-24 py-21 hero"
		style="background-image: url(<?php echo $hero_image['url']; ?>)">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">

					<header class="text-center text-white">
						<h1 class="mb-0 font-caflisch font-semibold text-6xl">
							<?php echo $hero_title ? $hero_title : get_the_title(); ?>
						</h1>
					</header>
					<div class="font-caflisch breadcrumbs text-2xl">
						<?php echo get_template_part('inc/breadcrumb'); ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
			
			<div class="mb-10 who-we-are">
				<div class="container">
					
					<div class="row">
						<div class="offset-lg-2 col-lg-4">
							<h2 class="font-caflisch mb-21 text-5xl">
								<?php echo $who_we_are['heading']; ?>
							</h2>

							<article class="text-gray-70">
								<?php echo wpautop( $who_we_are['content'] ); ?>
							</article>

							<div class="mb-10 border-t border-gray-5 mission-vission">
								<div class="toggle-view vission">
									<h3 class="mb-0 py-4 font-bold text-sm text-gray-90 text-uppercase cursor-pointer">
										<div class="flex justify-content-between align-items-center">
											<span>Vission</span>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dropdown-arrow.png" alt="">
										</div>
									</h3>
									<article class="text-gray-70 hidden">
										<?php echo wpautop( $who_we_are['vission'] ); ?>
									</article>
								</div>

								<div class="toggle-view mission">
									<h3 class="mb-0 py-4 font-bold text-sm text-gray-90 text-uppercase cursor-pointer">
										<div class="flex justify-content-between align-items-center">
											<span>Mission</span>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dropdown-arrow.png" alt="">
										</div>
									</h3>
									<article class="text-gray-70 hidden">
										<?php echo wpautop( $who_we_are['mission'] ); ?>
									</article>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<figure>
								<img src="<?php echo $who_we_are_image['url'] ?>" alt="<?php echo $who_we_are_image['alt'] ?>">
							</figure>
						</div>
					</div>

				</div> <!-- .container -->
			</div> <!-- .who-we-are -->

			<div class="services">
				<div class="container">
					<div class="py-12" style="background-color: <?php echo $services_bg_color; ?>">
						<div class="row">
							<div class="offset-lg-2 col-lg-8">

								<?php
									// Check rows exists.
									if( have_rows('field_61a891b371fbe') ):

										?>
										<div class="flex">
											<?php
												// Loop through rows.
												while( have_rows('field_61a891b371fbe') ) : the_row();

													// Load sub field value.
													$icon = get_sub_field('field_61a891c171fbf');
													$heading = get_sub_field('field_61a891c571fc0');
													$excerpt = get_sub_field('field_61a891cc71fc1');
													?>
														<div class="relative col-lg-4">
															<div class="text-center">
																<img class="mb-5" src="<?php echo $icon['url']; ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
																<h3 class="mb-4 text-2xl">
																	<?php echo $heading; ?>
																</h3>
																<div class="text-gray-70">
																	<?php echo wpautop( $excerpt ); ?>
																</div>
															</div>
														</div>
													<?php

												// End loop.
												endwhile;
											?>
										</div>
										<?php

									endif;
								?>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();