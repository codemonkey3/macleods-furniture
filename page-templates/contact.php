<?php
/**
 * Template Name: Contact Us
 * 
 * The template for displaying contact page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package macleods-furniture
 */

get_header(); 
// hero
$hero_image = get_field('field_61a88f4ee5f18');
$hero_title = get_field('field_61a88fb0e5f19');

// map
$map = get_field('field_61a892035cdca');

// connect with us
$connect_heading = get_field('field_61a8ab5697608');
$opening_hour_icon = get_field('field_61a8ab898ba35');
$opening_hour_heading = get_field('field_61a8925a5cdcd');

$locations_icon = get_field('field_61a8abb68ba3a');

// get in touch
$get_in_touch_bg_color = get_field('field_61a8b478e8476');
$get_in_touch_heading = get_field('field_61a8b46ee8475');
$get_in_touch_shortcode = get_field('field_61a8b4556d7a2');
?>

	<div class="mb-12 py-21 hero"
		style="background-image: url(<?php echo $hero_image['url']; ?>)">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					
					<header class="text-center text-white">
						<h1 class="mb-0 font-caflisch font-semibold text-6xl">
							<?php echo $hero_title ? $hero_title : get_the_title(); ?>
						</h1>
					</header>
					<div class="font-caflisch breadcrumbs text-2xl">
						<?php echo get_template_part('inc/breadcrumb'); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
			
			<div class="mb-24 map">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-lg-12">

							<?php echo $map; ?>

						</div>
					</div>

				</div> <!-- .container -->
			</div>

			<div class="connect-with-us">
				<div class="container">
					<div class="row">
						<div class="offset-lg-2 col-lg-8">
							<h2 class="font-caflisch mb-21 text-5xl">
								<?php echo $connect_heading; ?>
							</h2>

							<div class="mb-10 pb-8 flex border-b border-gray-5 opening-hours">
								<div class="mr-6" 
									style="flex: 0 0 42px;max-width: 42px;">
									<img src="<?php echo esc_url( $opening_hour_icon['url'] ); ?>" alt="">
								</div>
								<div>
									<h3 class="font-bold text-sm">
										<?php echo $opening_hour_heading; ?>
									</h3>
									<div class="row">
										<?php
											// Check rows exists.
											if( have_rows('field_61a899555cdce') ):

												// Loop through rows.
												while( have_rows('field_61a899555cdce') ) : the_row();

													// Load sub field value.
													$day_time = get_sub_field('field_61a8996b5cdcf');
													?>
													<div class="col-lg-3">
														<time class="text-gray-70 text-sm"><?php echo $day_time; ?></time>
													</div>
													<?php

												// End loop.
												endwhile;

											endif;
										?>
									</div>
								</div>
							</div>

							<div class="mb-14 flex border-b border-gray-5 office-locations">
								<div class="mr-6"
									style="flex: 0 0 42px;max-width: 42px;">
									<img src="<?php echo esc_url( $locations_icon['url'] ); ?>" alt="">
								</div>
								<div>
									<div class="row">
										<?php
											// Check rows exists.
											if( have_rows('field_61a8998d5cdd1') ):

												// Loop through rows.
												while( have_rows('field_61a8998d5cdd1') ) : the_row();

													// Load sub field value.
													$location_details = get_sub_field('field_61a8ac51d6854');
													$location_photo = get_sub_field('field_61a899e25cdd5');
													?>
													<div class="pl-0 pr-0 col-lg-6">
														<div class="pb-10 flex">
															<div class="col-lg-6">
																<h3 class="font-bold text-sm">
																	<?php echo $location_details['name']; ?>
																</h3>
																<article class="text-gray-70 text-sm">
																	<?php echo wpautop( $location_details['details'] ); ?>
																</article>
															</div>
															<div class="col-lg-6">
																<figure>
																	<img src="<?php echo $location_photo['url']; ?>" alt="">
																</figure>
															</div>
														</div>
													</div>													
													<?php

												// End loop.
												endwhile;
												
											endif;
										?>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div> <!--.connect-with-us-->

			<div class="py-13 get-in-touch"
				style="background-color: <?php echo $get_in_touch_bg_color; ?>">
				<div class="container">
					<div class="row">
						<div class="offset-lg-2 col-lg-8">

							<h2 class="font-caflisch mb-21 text-5xl text-white">
								<?php echo $get_in_touch_heading; ?>
							</h2>
							<?php echo do_shortcode( $get_in_touch_shortcode ); ?>
							
						</div>
					</div>
				</div>
			</div> <!--.get-in-touch-->

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();