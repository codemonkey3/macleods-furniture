<?php
/**
 * Template Name: FAQs
 * 
 * The template for displaying faqs pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package macleods-furniture
 */

get_header();

// hero
$hero_image = get_field('field_61a88f4ee5f18');
$hero_title = get_field('field_61a88fb0e5f19');

// faqs

?>

	<div class="mb-12 py-21 hero"
		style="background-image: url(<?php echo $hero_image['url']; ?>)">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					
					<header class="text-center text-white">
						<h1 class="mb-0 font-caflisch font-semibold text-6xl">
							<?php echo $hero_title ? $hero_title : get_the_title(); ?>
						</h1>
					</header>
					<div class="font-caflisch breadcrumbs text-2xl">
						<?php echo get_template_part('inc/breadcrumb'); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="container">
				<div class="row">
					<div class="offset-lg-2 col-lg-8">

            <div class="entry-content">
							<?php
								the_content();
								
								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'macleods-furniture' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->

					</div>
				</div>
			</div> <!-- .container -->

      <div class="faqs">
        <div class="container">
          <div class="row">
            <div class="offset-lg-2 col-lg-8">

              <?php
                // Check rows exists.
                if( have_rows('field_61b9efc1fa2c1') ):

                  // Loop through rows.
                  while( have_rows('field_61b9efc1fa2c1') ) : the_row();

                  // Load sub field value.
                  $heading = get_sub_field('field_61b9efdbfa2c2');
                  $excerpt = get_sub_field('field_61b9f15320cde');
                    ?>
                      <div class="mb-8 item">
                        <h3 class="font-bold text-uppercase"><?php echo $heading; ?></h3>
                        
                        <?php if( $excerpt ) { ?>
                          <article>
                            <?php echo wpautop( $excerpt ); ?>
                          </article>
                        <?php } ?>

                        <?php
                          // Check rows exists.
                          if( have_rows('field_61b9eff1fa2c3') ):

                            // Loop through rows.
                            while( have_rows('field_61b9eff1fa2c3') ) : the_row();

                              // Load sub field value.
                              $question = get_sub_field('field_61b9f004fa2c4');
                              $answer = get_sub_field('field_61b9f008fa2c5');
                              ?>
                                <details class="mb-2">
                                  <summary class="font-bold text-md"><?php echo $question; ?></summary>
                                  <article class="pt-2 pl-5">
                                    <?php echo wpautop( $answer ); ?>
                                  </article>
                                </details>
                              <?php

                              // End loop.
                            endwhile;
                          endif;
                        ?>
                      </div>
                    <?php
                  // End loop.
                  endwhile;
                endif;
              ?>

            </div>
          </div>
        </div>
      </div>

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();