<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package macleods-furniture
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			<div class="offset-lg-2 col-lg-8">
				
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
						
						<section class="mt-12 error-404 not-found">
							<header class="page-header text-center">
								<h1 class="page-title"><?php _e( '😞 Error <strong class="fw-900">404</strong>', 'macleods-furniture' ); ?></h1>
							</header><!-- .page-header -->
							
							<div class="page-content text-center">
								<p><?php esc_html_e( 'It looks like nothing was found at this location. We gonna fire a developer here, huh?', 'macleods-furniture' ); ?></p>
								<p><a class="btn" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Visit shop</a></p>
							</div><!-- .page-content -->
						</section><!-- .error-404 -->
						
					</main><!-- #main -->
				</div><!-- #primary -->

			</div>
		</div>
		
	</div> <!-- .container -->

<?php
get_footer();