<?php
class MF_Product_Color_Filter extends WP_Widget {
	
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
			'mf-product-color-filter', // Base ID
			'MF: Product Color Filter', // Name
			[ 'description' => __( 'Display the product color filter.', THEME_DOMAIN ) ] // Args
		);

		/* enable embeed style */
		add_action( 'wp_enqueue_scripts', array( $this, 'mf_product_filter_enqueue_assets' ) );
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;

		if ( ! empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}
		
		// set filter default
		$current_term = '';
		if( get_queried_object() ) {
			$current_term = isset( get_queried_object()->slug ) ? get_queried_object()->slug : '';
		}
		
		$attribute = 'color';
		$taxonomy = 'pa_color';
		
		$terms = get_terms( array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
		) );
		
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			// base link decided by current page
			if( $current_term ) {
				$link = get_term_link( $current_term, 'product_cat' );
			}
			else {
				$link = get_permalink( wc_get_page_id( 'shop' ) );
			}
			
			// get current chosen attributes
			$chosen_attributes = mf_chosen_attributes();
			?>
			<ul>
				<?php
					foreach( $terms as $term ) {
						// skip the term for the current archive
						if ( $current_term == $term->slug ) {
							continue;
						}
						
						$arg = 'filter_' . sanitize_title( $attribute );
						$current_filter = ( isset( $_GET[ $arg ] ) ) ? explode( ',', $_GET[ $arg ] ) : [];

						if ( ! is_array( $current_filter ) ) {
							$current_filter = [];
						}
						
						$current_filter = array_map( 'esc_attr', $current_filter );
						
						if ( ! in_array( $term->slug, $current_filter ) ) {
							$current_filter[] = $term->slug;
						}

						// All current filters
						if ( $chosen_attributes ) {
							foreach ( $chosen_attributes as $attribute_name => $data ) {
								if ( $attribute_name !== $taxonomy ) {

									// Exclude query arg for current term archive term
									while ( in_array( $current_term, $data['terms'] ) ) {
										$key = array_search( $current_term, $data );
										unset( $data['terms'][$key] );
									}
									
									// Remove pa_ and sanitize
									$filter_name = sanitize_title( str_replace( 'pa_', '', $attribute_name ) );

									if ( ! empty( $data['terms'] ) ) {
										$link = add_query_arg( 'filter_' . $filter_name, implode( ',', $data['terms'] ), $link );
									}

								}
							}
						}
						
						// Current Filter = this widget
						if ( 
							isset( $chosen_attributes[ $taxonomy ] ) 
							&& is_array( $chosen_attributes[ $taxonomy ]['terms'] ) 
							&& in_array( $term->slug, $chosen_attributes[ $taxonomy ]['terms'] ) 
						) {
							
							$class = 'class="chosen"';

							// remove this term if $current_filter has more than 1 term filtered
							if ( sizeof( $current_filter ) > 1 ) {
								$current_filter_without_this = array_diff( $current_filter, [$term->slug] );
								$link = add_query_arg( $arg, implode( ',', $current_filter_without_this ), $link );
							}

						}
						else {

							$class = '';
							$link = add_query_arg( $arg, implode( ',', $current_filter ), $link );

						}
						?>
						
						<li>
							<a rel="nofollow" <?php echo $class; ?> href="<?php echo esc_url( $link ); ?>">
								<?php $thumbnail_url = get_field('field_61b5ef3cbba03', $term); ?>
								<img src="<?php echo esc_url( $thumbnail_url['url'] ); ?>" alt="<?php echo esc_attr( $term->name ); ?>" />
							</a>
						</li>
						<?php
					}
				?>
			</ul>
			<?php
		}
		
		echo $after_widget;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'MF: Product Color Filter';
		
		?>
		<div class="widget-body">
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', THEME_DOMAIN ); ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
			</p>
		</div>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = [];
		$instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
	
	// enqueue scripts and style
	public function mf_product_filter_enqueue_assets() {
		if( is_active_widget( false, false, 'mf-product-color-filter', true ) ) {
			wp_enqueue_style( 'mf-product-color-filter-style', get_template_directory_uri() . '/lib/mf-product-color-filter/mf-product-color-filter.css', false, '1.0.0', 'all' );
		}
	}
	
}