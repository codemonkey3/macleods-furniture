<?php

add_action( 'widgets_init', 'macleods_furniture_load_widgets' );
/*
 * register theme widgets
 */
function macleods_furniture_load_widgets(){
	include_once( TEMPLATEPATH . '/lib/mf-product-color-filter/mf-product-color-filter.php' );
	
	register_widget( 'MF_Product_Color_Filter' );
}