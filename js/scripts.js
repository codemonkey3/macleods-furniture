/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function () {
  var container, button, menu, links, subMenus, i, len;

  container = document.getElementById("site-navigation");
  if (!container) {
    return;
  }

  button = container.getElementsByTagName("button")[0];
  if ("undefined" === typeof button) {
    return;
  }

  menu = container.getElementsByTagName("ul")[0];

  // Hide menu toggle button if menu is empty and return early.
  if ("undefined" === typeof menu) {
    button.style.display = "none";
    return;
  }

  menu.setAttribute("aria-expanded", "false");
  if (-1 === menu.className.indexOf("nav-menu")) {
    menu.className += " nav-menu";
  }

  button.onclick = function () {
    if (-1 !== container.className.indexOf("toggled")) {
      container.className = container.className.replace(" toggled", "");
      button.setAttribute("aria-expanded", "false");
      menu.setAttribute("aria-expanded", "false");
    } else {
      container.className += " toggled";
      button.setAttribute("aria-expanded", "true");
      menu.setAttribute("aria-expanded", "true");
    }
  };

  // Get all the link elements within the menu.
  links = menu.getElementsByTagName("a");
  subMenus = menu.getElementsByTagName("ul");

  // Set menu items with submenus to aria-haspopup="true".
  for (i = 0, len = subMenus.length; i < len; i++) {
    subMenus[i].parentNode.setAttribute("aria-haspopup", "true");
  }

  // Each time a menu link is focused or blurred, toggle focus.
  for (i = 0, len = links.length; i < len; i++) {
    links[i].addEventListener("focus", toggleFocus, true);
    links[i].addEventListener("blur", toggleFocus, true);
  }

  /**
   * Sets or removes .focus class on an element.
   */
  function toggleFocus() {
    var self = this;

    // Move up through the ancestors of the current link until we hit .nav-menu.
    while (-1 === self.className.indexOf("nav-menu")) {
      // On li elements toggle the class .focus.
      if ("li" === self.tagName.toLowerCase()) {
        if (-1 !== self.className.indexOf("focus")) {
          self.className = self.className.replace(" focus", "");
        } else {
          self.className += " focus";
        }
      }

      self = self.parentElement;
    }
  }
})();

/**
 * @desc    This contains all scripts use in site themes
 */
(function ($) {
  "use strict";

  if(window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
  }
  if(window.HTMLCollection && !HTMLCollection.prototype.forEach) {
    HTMLCollection.prototype.forEach = Array.prototype.forEach;
  }

  /**
   * All of the code for your public-facing JavaScript source
   * should reside in this file.
   *
   * Note: It has been assumed you will write jQuery code here, so the
   * $ function reference has been prepared for usage within the scope
   * of this function.
   *
   * This enables you to define handlers, for when the DOM is ready:
   *
   * $(function() {
   *
   * });
   *
   * When the window is loaded:
   *
   * $( window ).load(function() {
   *
   * });
   *
   * ...and/or other possibilities.
   *
   * Ideally, it is not considered best practise to attach more than a
   * single DOM-ready or window-load handler for a particular page.
   * Although scripts in the WordPress core, Plugins and Themes may be
   * practising this, we should strive to set a better example in our own work.
   */

  /* Process mobile menu */
  function rstheme_deal_with_mobile_nav() {
    if ($("body").hasClass("slide-menu-active")) {
      $("body").removeClass("slide-menu-active");
    } else {
      // Set up our sub-menus
      var el = $('<span class="mobiMenuChild"></span>');

      if (el.parent().length === 0) {
        $("#primary-menu li.menu-item-has-children").append(el);
      }

      // Hide the sub-menus
      $("#primary-menu li.menu-item-has-children").toggleClass("hidey", true);

      $("body").addClass("slide-menu-active");

      $("html, body").animate(
        {
          scrollTop: $(".site-header").offset().top,
        },
        200
      );
    }
  }

  $(".menu-toggle, .close-menu-btn").click(function (event) {
    event.preventDefault();
    rstheme_deal_with_mobile_nav();
  });

  $(document).on("click", ".mobiMenuChild", function (event) {
    event.stopPropagation();
    event.preventDefault();

    if (event.handled !== true) {
      $(this).parent().toggleClass("hidey");

      event.handled = true;
    } else {
      return false;
    }
  });

  // when the window is loaded:
  $(window).load(function() {
    $(".quantity .qty").attr("type", "text");
  });

  // Load below once the DOM is ready
  $(function() {

    // Sticky Menu
    $(".main-navigation").stick_in_parent({
      offset_top: 0
    });
    
    /**
     * Home
     */
    let heroSlider = document.querySelector('.hero-slider');
    if( heroSlider ) {
      $('.hero-slider').slick({
        infinite: true,
        slidesToShow: 1,
        dots: true,
      });
    }

    /**
     * WooCommerce
     */

    // product quantity
    $('form.cart').on( 'click', 'button.plus, button.minus', function() {
      var qty     = $(".qty"),
        val     = parseFloat(qty.val()),
        max     = parseFloat(qty.attr( 'max' )),
        min     = parseFloat(qty.attr( 'min' )),
        step    = parseFloat(qty.attr( 'step' ));

      if( isNaN(max) ) {
        max = 99;
      }
      
      if( isNaN(min) ) {
        min = 1;
      }

      // Change the value if plus or minus
      if ( $( this ).is( '.plus' ) ) {
        if ( max && ( max <= val ) ) {
          qty.val( max );
        }
        else {
          qty.val( val + step );
        }
      } else {
        if ( min && ( min >= val ) ) {
          qty.val( min );
        }
        else if ( val > 1 ) {
          qty.val( val - step );
        }
      }
    });

    // cart
    $('.woocommerce-cart-form').on( 'click', '.qty_button', function() {

      // Remove disabled properties
      $('.button, .update-cart-btn').removeAttr('disabled');

      var qty     = $(this).parents(".product-quantity").find(".qty"),
        val     = parseFloat(qty.val()),
        max     = parseFloat(qty.attr( 'max' )),
        min     = parseFloat(qty.attr( 'min' )),
        step    = parseFloat(qty.attr( 'step' ));
      
      // Change the value if plus or minus
      if ( $( this ).is( '.plus' ) ) {
        if ( max && ( max <= val ) ) {
          qty.val( max );
        }
        else {
          qty.val( val + step );
        }
      } 
      else {
        if ( min && ( min >= val ) ) {
          qty.val( min );
        }
        else if ( val > 1 ) {
          qty.val( val - step );
        }
      }
      
    });

    // shop, category page
    let productCategoriesListSubCat = document.querySelector('.wc-block-product-categories-list > li > ul');
    if( productCategoriesListSubCat ) {
      let tempSpan = document.createElement('span'); 
        tempSpan.classList.add('toggle-categories');
      
      productCategoriesListSubCat.insertAdjacentElement('beforebegin', tempSpan);
    }

    let toggleCategories = document.querySelectorAll('.toggle-categories');
    if( toggleCategories.length > 0 ) {
      toggleCategories.forEach( el => {
        el.addEventListener('click', e => {
          el.classList.toggle('active');
          
          el.nextElementSibling.classList.toggle('visible');
        });
      });
    }

    // shop, cateogry display filter
    let productDisplayFilter = document.querySelectorAll('.product-display-filter a');
    if( productDisplayFilter.length > 0 ) {
      productDisplayFilter.forEach(el => {
        el.addEventListener('click', e => {
          let display = el.getAttribute('data-id');

          localStorage.removeItem('product-display');
          localStorage.setItem( 'product-display', display );
          
          // remove old values
          document.body.classList.remove('product-display--grid');
          document.body.classList.remove('product-display--list');

          if( 'grid' == display ) {
            document.body.classList.add('product-display--grid');
          }
          else {
            document.body.classList.add('product-display--list');
          }
        });
      });
    }

  });

  // rapdio variations
  $(document).on('change', '.variation-radios input', function() {
    $('.variation-radios input:checked').each(function(index, element) {
      // remove old active items
      $('.variation-radios input').each(function(index, element) {
        let $el = $(element);
        $el.parent('label').removeClass('active');
      });

      var $el = $(element);
      var thisName = $el.attr('name');
      var thisVal  = $el.attr('value');
      
      $el.parent('label').addClass('active');
      
      $('select[name="'+thisName+'"]').val(thisVal).trigger('change');
    });
  });
  
  $(document).on('woocommerce_update_variation_values', function() {
    $('.variation-radios input').each(function(index, element) {
      var $el = $(element);
      var thisName = $el.attr('name');
      var thisVal  = $el.attr('value');
      $el.removeAttr('disabled');

      if($('select[name="'+thisName+'"] option[value="'+thisVal+'"]').is(':disabled')) {
        $el.prop('disabled', true);
      }
    });
  });

})(jQuery);

let init = () => {
  // Defer video / iframe and load only once the page is fully loaded
  deferVideo();

  // front-page
  ourProducts();

  // about-us
  aboutUs();
}

let deferVideo = () => {
  var vidDefer = document.getElementsByTagName('iframe');

  for (var i=0; i<vidDefer.length; i++) {
    if(vidDefer[i].getAttribute('data-src')) {
      vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
    } 
  }
}

let ourProducts = () => {
  let productFilters = document.querySelectorAll('.our-products .product-filter a');
  if( productFilters.length > 0 ) {
    productFilters.forEach(el => {
      el.addEventListener('click', e => {
        e.preventDefault();
        
        // hide previous items
        if( productFilters.length > 0 ) {
          productFilters.forEach(el => {
            el.classList.remove('active');
          });
        };

        // add class to the item
        el.classList.toggle('active');

        let catID = el.getAttribute('data-id'),
          productLists = document.querySelectorAll('.our-products .product-lists .item');

        // hide previous items
        if( productLists.length > 0 ) {
          productLists.forEach(el => {
            el.style.display = 'none';
          });
        };

        // show all product lists
        if( productLists.length > 0 ) {
          productLists.forEach(el => {
            let itemCat = el.getAttribute('data-id');
            
            if( catID == itemCat ) {
              el.style.display = 'block';
            }
          });
        };

      });
    });
  }
}

let aboutUs = () => {
  let toggles = document.querySelectorAll('.toggle-view h3');
  if( toggles ) {
    toggles.forEach(el => {
      el.addEventListener('click', e => {
        el.classList.toggle('active');
        el.nextElementSibling.classList.toggle('hidden');
      });
    });
  }
} 

window.addEventListener('load', init());

document.addEventListener('DOMContentLoaded', (event) => {
  let productDisplay = localStorage.getItem('product-display');
  
  switch( productDisplay ) {
    case "grid":
        document.body.classList.add('product-display--grid');
      break;

    case "list":
      document.body.classList.add('product-display--list');
    break;

    default:
        document.body.classList.add('product-display--grid');
      break;
  }
  
});