<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package macleods-furniture
 */

// get ACF value
$footer_top_logo = get_field('field_61a88240e019a', 'options');
$footer_top_contact_heading = get_field('field_61a8825be019c', 'options');
$footer_top_contact_details = get_field('field_61a88266e019d', 'options');

$footer_top_social_heading = get_field('field_61a88283e019f', 'options');

$footer_top_newsletter_heading = get_field('field_61a882c8e01a4', 'options');
$footer_top_newsletter_form = get_field('field_61a882dfe01a5', 'options');

// copyright
$copyright = get_field('field_61a88375fbf37', 'options');
?>
	</div><!-- #content -->

	<?php if( is_product() ) {
		
		// newsletter
		$newsletter_label = get_field('field_61b09dce4d164', 'options');
		$newsletter_form_shortcode = get_field('field_61b09def4d165', 'options');
		?>

		<?php
			/* woocommerce_related_products( [
				'posts_per_page' => 4,
				'columns'        => 4,
				'orderby'        => 'rand'
			] ); */

			global $product, $woocommerce_loop; 
			
			if ( ! $product ) { 
				return; 
			} 
			
			$defaults = [
				'posts_per_page' => 4,
				'columns'        => 4,
				'orderby'        => 'date',
				'order'			 => 'desc'
			]; 

			$args = wp_parse_args( $args, $defaults ); 

			// Get visble related products then sort them at random. 
			$args['related_products'] = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $args['posts_per_page'], $product->get_upsell_ids() ) ), 'wc_products_array_filter_visible' ); 
			
			// Handle orderby. 
			$args['related_products'] = wc_products_array_orderby( $args['related_products'], $args['orderby'], $args['order'] ); 

			// check if we have related product
			if( $args['related_products'] ) {
				?>
					<div class="related-products">

						<div class="container">
							<div class="row">
								<div class="offset-lg-2 col-lg-8">
									
									<?php
										// Set global loop values. 
										$woocommerce_loop['name'] 		= 'related'; 
										$woocommerce_loop['columns'] 	= apply_filters( 'woocommerce_related_products_columns', $args['columns'] ); 
										
										wc_get_template( 'single-product/related.php', $args );
									?>

								</div>
							</div>
						</div>

					</div> <!--.related-products-->
				<?php
			}
		?>

		<div class="mt-12 mailing-list">
			<div class="container">
				
				<div class="row">
					<div class="offset-lg-2 col-lg-8">
						<div class="p-8 bg-blue">
							<div class="flex justify-content-between align-items-center">
								<div class="p-0 col-lg-7">
									<h3 class="mb-0 font-bold text-white"><?php echo $newsletter_label; ?></h3>
								</div>
								<div class="p-0 col-lg-5">
									<?php echo do_shortcode( $newsletter_form_shortcode ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div> <!--.mailing-list-->
	<?php } ?>

	<div class="py-12 payment-partners">
		<div class="container">
			<div class="row">

					<?php
						// Check rows exists.
						if( have_rows('field_61a8813623b29', 'options') ):

							$item_counter = 0;

							// Loop through rows.
							while( have_rows('field_61a8813623b29', 'options') ) : the_row();

								// Load sub field value.
								$logo = get_sub_field('field_61a8813e23b2a');
								?>
									<div class="<?php echo $item_counter == 0 ? 'offset-lg-3' : ''; ?> col-lg-2">
										<img class="p-7 border border-1 block object-contain" src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php echo $logo['alt'] ?>" />
									</div>
								<?php

								// increment counter
								$item_counter++;

							// End loop.
							endwhile;

						endif;
					?>

				</div>
			</div>
		</div>
	</div> <!-- .payment-partners -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-top">
		  <div class="container">

				<div class="px-10 py-12 bg-gray">
					<div class="row">
						<div class="col-lg-3">
							<figure class="mb-0 footer-logo">
								<img src="<?php echo esc_url( $footer_top_logo['url'] ) ?>" alt="<?php echo esc_url( $footer_top_logo['alt'] ) ?>">
							</figure>
						</div>

						<div class="col-lg-3">
							<div class="mb-0 widget">
								<h3 class="font-bold text-2xl text-white widget-title">
									<?php echo $footer_top_contact_heading; ?>
								</h3>
								<article>
									<?php echo wpautop( $footer_top_contact_details ); ?>
								</article>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="mb-0 widget">
								<h3 class="font-bold text-2xl text-white widget-title">
									<?php echo $footer_top_social_heading; ?>
								</h3>

								<?php
									// Check rows exists.
									if( have_rows('field_61a88296e01a0', 'options') ):
										?>
										<ul class="ml-0 pl-0 flex align-items-center">
											<?php
												// Loop through rows.
												while( have_rows('field_61a88296e01a0', 'options') ) : the_row();

													// Load sub field value.
													$icon = get_sub_field('field_61a882a3e01a1');
													$link = get_sub_field('field_61a882a5e01a2');
													?>
													<li class="mr-5">
														<a href="<?php echo esc_url( $link['url'] ) ?>">
															<img src="<?php echo esc_url( $icon['url'] ); ?>" alt="<?php echo esc_url( $icon['alt'] ); ?>">
														</a>
													</li>
													<?php

												// End loop.
												endwhile;
											?>
										</ul>
										<?php

									endif;
								?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="mb-0 widget">
								
								<div class="newsletter">
									<h3 class="font-bold text-2xl text-white widget-title">
										<?php echo $footer_top_newsletter_heading; ?>
									</h3>

									<?php echo do_shortcode( $footer_top_newsletter_form ); ?>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div> <!-- .footer-top -->

		<div class="py-7 footer-bottom">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-6">
            <?php
              wp_nav_menu( array( 
                'theme_location' => 'footer_menu',
                'menu_id' 		 => 'footer-menu',
                'menu_class' 	 => 'footer-menu list-none ml-0 mb-0 pl-0 flex justify-content-start text-gray-90',
                'container' 	 => 'ul'
              ) );
            ?>
          </div>
          <div class="col-lg-6">
            <p class="mb-0 text-right text-gray-70 copyright">
              &copy; <?php echo date('Y') .' '. $copyright; ?>
            </p>
          </div>
        </div>
      </div>
		</div> <!-- .footer-bottom -->
	</footer><!-- #colophon -->

	<div class="slide-menu">
		<a href="javascript: void(0);" class="close-menu-btn">
			<span></span>
			<span></span>
		</a>
		
		<nav id="mobile-navigation" class="mobile-navigation" role="navigation">						
			<?php wp_nav_menu( array( 
				'theme_location' => 'mobile-menu',
				'menu_id' 		 => 'primary-menu',
				'menu_class' 	 => 'primary-menu',
				'container' 	 => 'ul'
			) ); ?>
		</nav><!-- #site-navigation -->
	</div><!-- .mobile-menu -->
	
</div><!-- #page -->

<?php
	// include WP footer for hook and styles
	wp_footer();

	// display tracking or analytics code
	if( isset( $options['analytics'] ) ) {
		echo $options['analytics'];
	}
?>

</body>
</html>