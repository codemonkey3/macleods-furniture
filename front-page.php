<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// get ACF value
$product_id = get_field('field_61a8c793d4b2c');

// our products
$our_product_bg_color = get_field('field_61ac8e15406ec');
$our_product_heading = get_field('field_61ac8bc84230b');
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="my-12 product-categories">
				<div class="container">

					<div class="lg:flex overflow-scroll">
						<div class="col-lg-4">
							<?php
								$product_category_ids = get_field('field_61a8c770b8e0c');
								if( $product_category_ids ) {
									$prouct_cat_counter = 0;
									
									foreach( $product_category_ids as $product_cat_id ) {
										$category = get_term( $product_cat_id, 'product_cat' );

										?>		
											<a class="mb-8 block" href="<?php echo esc_url( get_category_link( $product_cat_id ) ); ?>">
												<div class="py-4 bg-gray-5 text-center">
													<?php
														$thumbnail_id = get_term_meta( $product_cat_id, 'thumbnail_id', true );
														$thumbnail_url = wp_get_attachment_url( $thumbnail_id );
														if( $thumbnail_url ) {
															?>
															<figure class="mb-0">
																<img class="object-contain" src="<?php echo esc_url($thumbnail_url); ?>" alt="<?php echo esc_attr( $category->name ); ?>">
															</figure>
															<?php
														}
													?>
													
													<h3 class="mb-0 font-light font-caflisch text-6xl text-black">
														<?php 
															if ( $category || ! is_wp_error( $category ) ) {
																echo $category->name;
															}
														?>
													</h3>
												</div>
											</a>
										<?php

										// increment counter
										$prouct_cat_counter++;

										if( $prouct_cat_counter % 2 == 0 ) {
											$prouct_cat_counter = 0;
											?>
											</div><div class="col-lg-4">
											<?php
										}
										
									}
										
								}
							?>
						</div>
					</div>

				</div> <!-- .container -->
			</div> <!--.product-categories-->
			
			<div class="pt-8 pb-12 featured-product">
				<div class="container">
					
					<?php
						$product = wc_get_product( $product_id );
						if( $product ) {
							
							?>
							
								<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

									<div class="row">
										<div class="offset-lg-2 col-lg-3">
											<div class="summary entry-summary">
												<h2 class="mb-6 text-4xl">
													<?php echo $product->get_title(); ?>
												</h2>
												
												<article class="text-gray-70">
													<?php echo $product->get_short_description(); ?>
												</article>
												
												<?php
												/**
												 * Hook: woocommerce_single_product_summary.
												 *
												 * @hooked woocommerce_template_single_title - 5
												 * @hooked woocommerce_template_single_rating - 10
												 * @hooked woocommerce_template_single_price - 10
												 * @hooked woocommerce_template_single_excerpt - 20
												 * @hooked woocommerce_template_single_add_to_cart - 30
												 * @hooked woocommerce_template_single_meta - 40
												 * @hooked woocommerce_template_single_sharing - 50
												 * @hooked WC_Structured_Data::generate_product_data() - 60
												 */
												do_action( 'woocommerce_single_product_summary' );
												?>
											</div>
										</div>

										<div class="offset-lg-1 col-lg-4">
											<figure>
												<?php
													echo $product->get_image('full');
												?>
											</figure>
										</div>
									</div>

								</div>
								
							<?php
						}
					?>

				</div> <!-- .container -->
			</div> <!--.featured-product-->

			<?php
				// product categories
				$product_category_ids = get_field('field_61a8c7c2d4b2e');
			?>
			<div class="pt-12 pb-8 our-products"
				style="background-color: <?php echo $our_product_bg_color; ?>">
				<div class="container">

					<div class="row">
						<div class="offset-lg-2 col-lg-8">

							<div class="mb-12 product-filter align-items-center">
								<div class="flex justify-content-between align-items-center">
									<h2 class="mb-0 font-caflisch text-6xl">
										<?php echo $our_product_heading; ?>
									</h2>
									<div>
										<ul class="mb-0 ml-0 pl-0 flex justify-content-end list-none">
											<?php
												if( $product_category_ids ) {
													foreach( $product_category_ids as $key => $product_cat_id ) {
														$category = get_term( $product_cat_id, 'product_cat' );
														?>
														<li class="ml-4">
															<a class="relative font-bold text-sm text-black text-uppercase <?php echo 0 == $key ? 'active' : ''; ?>" 
																href="javascript: void(0);" 
																data-id="<?php echo $category->term_id; ?>">
																<span><?php echo $category->name; ?></span>
															</a>
														</li>
														<?php
													}
												}
											?>
										</ul>
									</div>
								</div>
							</div>

							<div class="product-lists">
								<div class="row">
									<?php
										if( $product_category_ids ) {
											foreach( $product_category_ids as $key => $product_cat_id ) {
												// $category = get_term( $product_cat_id, 'product_cat' );
												
												$product_args = [
													'post_type' => 'product',
													'tax_query' => [
														[
															'taxonomy' => 'product_cat',
															'terms'    => $product_cat_id,
														]
													],
													'posts_per_page' => 8
												];

												$products = new WP_Query( $product_args );
												if( $products->have_posts() ) {
													while( $products->have_posts() ) { $products->the_post();

														$product = wc_get_product( get_the_ID() )
														?>
															<div class="col-lg-3 item"
																data-id="<?php echo $product_cat_id; ?>" style="<?php echo 0 != $key ? 'display: none;' : '' ?>">
																<div class="mb-12">
																	<a class="text-black hover:text-black" href="<?php the_permalink(); ?>">
																		<figure>
																			<?php echo woocommerce_get_product_thumbnail(); ?>
																		</figure>
																		
																		<h3 class="mb-1 text-base">
																			<?php echo the_title(); ?>
																		</h3>
																		
																		<p class="font-bold text-xl mb-0">
																			<?php echo $product ? $product->get_price_html() : 0; ?>
																		</p>
																	</a>
																</div>
															</div>
														<?php
													}
												}
											}
										}
									?>
								</div>
							</div>

						</div>
					</div>

				</div> <!-- .container -->
			</div> <!-- .our-products -->
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();